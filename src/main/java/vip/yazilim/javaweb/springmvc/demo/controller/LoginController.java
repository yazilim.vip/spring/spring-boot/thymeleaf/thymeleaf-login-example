package vip.yazilim.javaweb.springmvc.demo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

    @GetMapping("/login")
    //@RequestMapping(value="/login",method=RequestMethod.GET);
    public String login() {
        return "login";
    }

    @PostMapping("/doLogin")
    //@RequestMapping(value="/doLogin",method=RequestMethod.POST);
    public String doLogin(@RequestParam String username
            , @RequestParam String password, ModelMap model) {
        if(username.equals("yazilim") && password.equals("vip")){
            model.addAttribute("username",username);
            return "home";
        }else{
            model.addAttribute("errorMessage","invalid user");
            model.addAttribute("username",username);
            return "redirect:/login";
        }
    }

}